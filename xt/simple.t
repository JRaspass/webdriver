use Test;
use WebDriver;

constant $url = 'http://httpd/';

my $wd = WebDriver.new: :4444port, :host<geckodriver>,
    :capabilities(:alwaysMatch('moz:firefoxOptions' => :args('-headless',)));

$wd.get: $url;

is $wd.find('h3').text, 'foo', 'find';

is-deeply $wd.find-all('h3')».text, <foo bar>, 'find-all';

is $wd.find('body').find('h3').text, 'foo', 'find.find';

is-deeply $wd.find('body').find-all('h3')».text, <foo bar>, 'find.find-all';

like $wd.source, / '<html>' /, 'source';

is-deeply $wd.status, { :message('Session already started'), :!ready },
    'status';

is $wd.title, 'Frosty the ☃', 'title';

is $wd.url, $url, 'url';

lives-ok { $wd.delete-session }, 'delete-session';
lives-ok { $wd.delete-session }, 'delete-session twice is a no-op';

throws-like { $wd.title }, Exception,
    :message(/'invalid session id: Tried to run command without establishing a connection'/),
    'title after delete-session throws';

done-testing;
