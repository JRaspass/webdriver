use Test;
use WebDriver;

my $wd = WebDriver.new: :4444port, :host<geckodriver>,
    :capabilities(:alwaysMatch('moz:firefoxOptions' => :args('-headless',)));

$wd.get: 'http://httpd';

$wd.window-rect :640width :480height;

is-deeply $wd.window-rect, { :0x :0y :640width :480height },
    'window-rect :640width :480height';

$wd.window-rect :10x :20y :800width :600height;

is-deeply $wd.window-rect, { :10x :20y :800width :600height },
    'window-rect :10x :20y :800width :600height';

$wd.window-maximize;

is-deeply $wd.window-rect, { :0x :0y :1366width :768height },
    'window-maximize';

my $new-window = $wd.window-new;

is-deeply $wd.window-handles, [ $wd.window-handle, $new-window ],
    'window-handles';

$wd.window-switch: $new-window;

is-deeply $wd.window-handle, $new-window, 'window-handle';

$wd.window-close;

done-testing;
